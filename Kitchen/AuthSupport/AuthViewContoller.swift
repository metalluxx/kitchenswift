//
//  AuthViewContoller.swift
//  Kitchen
//
//  Created by Metalluxx on 09/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import QuartzCore

@IBDesignable
class AuthViewContoller: UIViewController {
    
    @IBOutlet weak var topPanel: UILabel!
    @IBOutlet weak var bottomPanel: UILabel!
    @IBOutlet weak var labelApp: UILabel!
    @IBOutlet weak var labelStack: UIStackView!
    @IBOutlet weak var labelVisualEffectLabel: UIVisualEffectView!{
        didSet{
            labelVisualEffectLabel.layer.shadowColor = UIColor.black.cgColor
            labelVisualEffectLabel.layer.shadowOpacity = 0.6
            labelVisualEffectLabel.layer.shadowRadius = 0
        }
    }
    @IBOutlet weak var buttonLogin: UIButton!{
        didSet {
            buttonLogin.layer.cornerRadius = 5
            buttonLogin.layer.borderWidth = 2
            buttonLogin.layer.borderColor = UIColor.orange.cgColor
        }
    }
    
    @IBOutlet weak var fieldUsername: UITextField! {
        didSet{
            fieldUsername.layer.shadowColor = UIColor.black.cgColor
            fieldUsername.layer.shadowOpacity = 0.6
            fieldUsername.layer.shadowRadius = 15
        }
    }
    
    @IBOutlet weak var fieldPassword: UITextField! {
        didSet {
            fieldPassword.layer.shadowColor = UIColor.black.cgColor
            fieldPassword.layer.shadowOpacity = 0.6
            fieldPassword.layer.shadowRadius = 15
        }
    }
    
    @IBOutlet weak var constraitLabel: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    @IBInspectable var statusBarStyle : UIStatusBarStyle = UIStatusBarStyle.lightContent
    
    var isOpenedKeyboard = false
    
    var animationTimer = Timer()

    deinit{
        self.disableObserveForKeyboard()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _init()
    }
    
    
    func _init() -> Void {
        
        self.enableObserverForKeyboard()
        AuthGradientLayerController.insertGradient(view: self.view)
        labelVisualEffectLabel.center = CGPoint(x: 160.75, y: 284.5)
        
        animationTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { (timer) in
            
            UIView.animate(withDuration: 1, animations: {
                self.labelVisualEffectLabel.layer.shadowRadius += 10
                self.labelVisualEffectLabel.transform = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
                self.labelVisualEffectLabel.center.y -= 20
                self.constraitLabel.constant -= 20

                if self.labelVisualEffectLabel.center.y < 150 {
                    timer.invalidate()
                }
            })
        })
        
        fieldUsername.text = "1"
        fieldPassword.text = "1"
        self.loginTapped(self.buttonLogin)
    }

    
    func enableObserverForKeyboard() -> Void {
        NotificationCenter.default.addObserver(self, selector: #selector(showedKeyboard(notification:)), name: UIResponder.keyboardWillShowNotification,  object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hiddenKeyboard), name: UIResponder.keyboardWillHideNotification , object: nil)
    }
    
    func disableObserveForKeyboard() -> Void {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func showedKeyboard(notification : Notification) -> Void {
        

        let frame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        var activeHeight: CGFloat {
            return ((self.view.frame.height) - frame.height)
        }
        var buttonHeight : CGFloat {
            return self.buttonLogin.frame.origin.y + self.buttonLogin.frame.height
        }
        if activeHeight < buttonHeight {
            self.scrollView.contentOffset = CGPoint(x: 0.0 , y: (buttonHeight - activeHeight + 15.0))
            
            if !isOpenedKeyboard {
                self.constraitLabel.constant -= self.scrollView.contentOffset.y
            }
        }
        
        isOpenedKeyboard = true
    }
    
    @objc func hiddenKeyboard() -> Void {
        if isOpenedKeyboard {
            self.constraitLabel.constant += self.scrollView.contentOffset.y
        }
        scrollView.contentOffset = CGPoint.zero
        isOpenedKeyboard = false
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        if (fieldUsername.text == "1") && (fieldPassword.text == "1") {
            self.performSegue(withIdentifier: "accessLogin", sender: self)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension AuthViewContoller : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.fieldUsername:
            self.fieldPassword.becomeFirstResponder()
            break
        default:
            loginTapped(self.buttonLogin)
        }
        return true
    }
}
