//
//  AuthGradientLayerController.swift
//  Kitchen
//
//  Created by Metalluxx on 10/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class AuthGradientLayerController: NSObject {
    
    static let c1 : UIColor = UIColor.init(red: 161 / 255, green: 127 / 255, blue: 186 / 255, alpha: 1.0)
    static let c2 : UIColor = UIColor.init(red: 97 / 255 , green: 79 / 255, blue: 127 / 255, alpha: 1.0)
    static let c3 : UIColor = UIColor.init(red: 78 / 255, green: 21 / 255, blue: 89 / 255, alpha: 1.0)
    
    static func insertGradient(view vw: UIView?) -> Void {
        if vw != nil {
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [c1.cgColor, c2.cgColor, c3.cgColor]
            gradientLayer.frame = vw!.bounds
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
            gradientLayer.locations = [0.0, 0.5, 1.0]

            vw!.layer.insertSublayer(gradientLayer, at: 0)
        }
    }
}
