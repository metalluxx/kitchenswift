//
//  HomeTabController.swift
//  Kitchen
//
//  Created by Metalluxx on 11/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class HomeTabController: UIViewController {

    
    @IBOutlet weak var tabItem: UITabBarItem! {
        didSet{
            
            let originImage = UIImage.init(named: "tabHome")
            tabItem.image = UIImage.init(cgImage: (originImage?.cgImage)!, scale: ((originImage?.size.height)!/30), orientation: (originImage?.imageOrientation)!)
            
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register((UINib.init(nibName: "ReceiptCell", bundle: nil)), forCellWithReuseIdentifier: "Receipt")

        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.topItem?.title = "Home"
        navigationController?.navigationBar.prefersLargeTitles = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension HomeTabController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : ReceiptCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "Receipt", for: indexPath) as? ReceiptCell
        cell!.nameView.text = "САСАЬ"
        cell!.imageView.image = UIImage.init(named: "tabHome")
        return cell!
    }
    
    
    
    
}
