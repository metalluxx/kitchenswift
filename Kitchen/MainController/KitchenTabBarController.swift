//
//  KitchenTabBarController.swift
//  Kitchen
//
//  Created by Metalluxx on 11/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class KitchenTabBarController: UITabBarController {
    
    
    
    @IBOutlet weak var navItem: UINavigationItem!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBar.appearance().contentMode = UIView.ContentMode.scaleAspectFit
        UITabBar.appearance().clipsToBounds = true
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
