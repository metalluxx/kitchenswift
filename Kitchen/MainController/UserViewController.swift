//
//  UserViewController.swift
//  Kitchen
//
//  Created by Metalluxx on 11/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {

    
    @IBOutlet weak var tabItem: UITabBarItem! {
        didSet{
            let originImage = UIImage.init(named:"tabUser")
            tabItem.image = UIImage.init(cgImage: (originImage?.cgImage)!, scale: ((originImage?.size.height)!/30), orientation: (originImage?.imageOrientation)!)
        }
    }
    
    @IBOutlet var tabView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.topItem?.title = "User"
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
