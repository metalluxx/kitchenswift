//
//  CollectionViewCell.swift
//  Kitchen
//
//  Created by Metalluxx on 11/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class ReceiptCell: UICollectionViewCell {
    
    var view : UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameView: UILabel!
    @IBOutlet weak var timeView: UILabel!
    
    @IBOutlet weak var mainView: UIView! {
        didSet{
            mainView.layer.shadowRadius = 8
            mainView.layer.shadowOpacity = 0.4
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
